package it.bitsplitters.cocinspector;

import static it.bitsplitters.cocinspector.Config.*;
import static it.bitsplitters.util.BotUtil.*;

import java.util.Optional;

import javax.security.auth.login.LoginException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.github.ygimenez.exception.InvalidHandlerException;
import com.github.ygimenez.method.Pages;
import com.github.ygimenez.model.Paginator;
import com.github.ygimenez.model.PaginatorBuilder;
import com.github.ygimenez.type.Emote;

import it.bitsplitters.BotManager;
import it.bitsplitters.command.ChannelEnabled;
import it.bitsplitters.command.exceptions.CommandPermissionException;
import it.bitsplitters.command.impl.CategorizedHelpCommand;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.events.guild.GuildJoinEvent;
import net.dv8tion.jda.api.events.guild.GuildLeaveEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
public class MessageListener extends ListenerAdapter
{
	
	private static final Logger logger = LogManager.getLogger();
	
	private BotManager botManager;
	
	public MessageListener() {
	    botManager = new BotManager(botConfig, commandsManager, usersManager, guildsManager,new CategorizedHelpCommand(ChannelEnabled.ALL,"<:menu:821097010362449926>|Menu", "__***CoC INSPECTOR***__|255,165,0|<:blank:802526354762498098>",commandsPagination), commands);
	}
	/**
     * This is the method where the program starts.
     */
    public static void main(String[] args)
    {
        //We construct a builder for a BOT account. If we wanted to use a CLIENT account
        //we would use AccountType.CLIENT
        try
        {
            JDA jda = JDABuilder.createDefault(getBotToken()) // The token of the account that is logging in.
                    .addEventListeners(new MessageListener())   // An instance of a class that will handle events.
                    .setActivity(Activity.watching("-help"))
                    .build();
            jda.awaitReady(); // Blocking guarantees that JDA will be completely loaded.
            
            logger.atInfo().log("Finished Building JDA!");
            
            Paginator paginator = PaginatorBuilder.createPaginator().setHandler(jda)
            		.shouldRemoveOnReact(true)
            		.setEmote(Emote.GOTO_FIRST,"815392649959702618")
            		.setEmote(Emote.PREVIOUS,"815392649674620929")
            		.setEmote(Emote.GOTO_LAST,"815392649728622613") 
            		.setEmote(Emote.NEXT,"815392649988538396")
            		.setEmote(Emote.CANCEL, "821080078884667423")
            		.build();
            
            Pages.activate(paginator);
            
            Config.setJDA(jda);
        }
        catch (LoginException e)
        {
            //If anything goes wrong in terms of authentication, this is the exception that will represent it
        	logger.atError().log(e.getMessage());
        }
        catch (InterruptedException e)
        {
            //Due to the fact that awaitReady is a blocking method, one which waits until JDA is fully loaded,
            // the waiting can be interrupted. This is the exception that would fire in that situation.
            //As a note: in this extremely simplified example this will never occur. In fact, this will never occur unless
            // you use awaitReady in a thread that has the possibility of being interrupted (async thread usage and interrupts)
        	logger.atError().log(e.getMessage());
        } catch (InvalidHandlerException e) {
        	logger.atError().log(e.getMessage());
			e.printStackTrace();
		}
    }
    
    @Override
    public void onGuildLeave(GuildLeaveEvent event) {
    	logger.atInfo().log("Server uscito! {}", event.getGuild().getName());
    	guildsManager.guildLeave(event.getGuild().getIdLong());
    	
    }
    
    @Override
    public void onGuildJoin(GuildJoinEvent event) {
    	logger.atInfo().log("Nuovo server! {}", event.getGuild().getName());
    	guildsManager.guildJoin(event.getGuild().getIdLong());
    }
    
    /**
     * @param event
     *          An event containing information about a {@link net.dv8tion.jda.api.entities.Message Message} that was
     *          sent in a channel.
     */
    @Override
    public void onMessageReceived(MessageReceivedEvent event) {

        if(event.getAuthor().isBot()) //This boolean is useful to determine if the User that
        	return;  	   //sent the Message is a BOT or not!
		
        Optional<Exception> optError = Optional.empty();
    	
        if (event.isFromType(ChannelType.TEXT))
    		optError = botManager.findAndRunGuildCommand(event);
    	
        if(event.isFromType(ChannelType.PRIVATE))
    		optError = botManager.findAndRunUserCommand(event);
        
            if(optError.isPresent()) 
            	if(optError.get().getMessage() != null) 
            		if(optError.get() instanceof CommandPermissionException) 
            			stopEmbed(event, optError.get().getMessage());
            		else 
            			warningEmbed(event, optError.get().getMessage());
            		
            	else
            		optError.get().printStackTrace();
            
    }
}
