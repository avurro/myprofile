package it.bitsplitters.cocinspector.util;

import static it.bitsplitters.cocinspector.Config.getMsg;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.*;
import static it.bitsplitters.util.BotUtil.*;

import java.awt.Color;
import java.net.http.HttpConnectTimeoutException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.bitsplitters.clashapi.exception.AuthenticationException;
import it.bitsplitters.clashapi.exception.BadRequestException;
import it.bitsplitters.clashapi.exception.MaintenanceException;
import it.bitsplitters.clashapi.exception.NotFoundException;
import it.bitsplitters.clashapi.exception.RateLimitExceededException;
import it.bitsplitters.cocinspector.Config;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Emote;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class MessageUtil {

	private static final Logger logger = LogManager.getLogger();
	
	public static Emote cocInspectorEmote = null;
	public static final String TH1 = "<:th1:819615768727519232>"; 
	public static final String TH2 = "<:th2:819615768513478726>"; 
	public static final String TH3 = "<:th3:819615768194056193>"; 
	public static final String TH4 = "<:th4:819615768807342080>"; 
	public static final String TH5 = "<:th5:819615768811143228>"; 
	public static final String TH6 = "<:th6:819615768542183425>"; 
	public static final String TH7 = "<:th7:819615768911413258>"; 
	public static final String TH8 = "<:th8:819615768630919199>"; 
	public static final String TH9 = "<:th9:819615768848105472>"; 
	public static final String TH10 = "<:th10:819615769054019644>"; 
	public static final String TH11 = "<:th11:819615768949817354>"; 
	public static final String TH12 = "<:th12:821076795796750447>"; 
	public static final String TH13 = "<:th13:820833665092288562>";
	public static final String TH14 = "<:th14:830857342719361134>";
	
	public static final String CMD_EMOJI = "<:cmd:797260301309181963>";
	public static final String CMD_SUB_EMOJI ="<:cmdsub:823694751411994655>";
	public static final String LOADING = "<a:loading:801962006729981992>";
	public static Color cocIspectorColor = new Color(255,165,0);
	
	// CON ACCODATO IL TAG QUESTO LINK APRE DIRETTAMENTE IL GAME NEL PROFILO DI INTERESSE
	public static final String inGamePlayerLink = "https://link.clashofclans.com/?action=OpenPlayerProfile&tag=";
		
	public static String getPlayerLink(String name, String tag) {
		return String.join("","[",name,"](",inGamePlayerLink,tag,")");
	}
	
	public static void setCocInspectorEmote() {
		 cocInspectorEmote = Config.JDA().getGuildById(747102960651075685L).getEmoteById(820816890615562321L);
	}
	
	public static String getTHemoji(int level) {
		switch (level) {
			case 1: return TH1;
			case 2: return TH2;
			case 3: return TH3;
			case 4: return TH4;
			case 5: return TH5;
			case 6: return TH6;
			case 7: return TH7;
			case 8: return TH8;
			case 9: return TH9;
			case 10: return TH10;
			case 11: return TH11;
			case 12: return TH12;
			case 13: return TH13;
			case 14: return TH14;
			
			default: return "";
		}
	}
	
	public static Message sendLoading(MessageReceivedEvent event) {
		EmbedBuilder eb = new EmbedBuilder();
		eb.setDescription("***Loading*** "+LOADING);
		eb.setColor(cocIspectorColor);
		return  event.getChannel().sendMessage(eb.build()).complete();
	}

	public static void stopMessage(MessageReceivedEvent event, String labelKey, String... values) {
		getMsg(event, labelKey)
		.ifPresent(msg -> stopEmbed(event, replaceValues(msg, values)));
	}
	
	public static void unauthorizedMessage(MessageReceivedEvent event, String labelKey, String... values) {
		getMsg(event, labelKey)
		.ifPresent(msg -> unauthorizedEmbed(event, replaceValues(msg, values)));
	}
	
	public static void infoMessage(MessageReceivedEvent event, String labelKey, String... values) {
		getMsg(event, labelKey)
		.ifPresent(msg -> infoEmbed(event, replaceValues(msg, values)));
	}
	
	public static void warningMessage(MessageReceivedEvent event, String labelKey, String... values) {
		getMsg(event, labelKey)
		.ifPresent(msg -> warningEmbed(event, replaceValues(msg, values)));
	}
	
	public static void okMessage(MessageReceivedEvent event, String labelKey, String... values) {
		getMsg(event, labelKey)
		.ifPresent(msg -> okEmbed(event, replaceValues(msg, values)));
	}
	
	public static void errorMessage(MessageReceivedEvent event, String labelKey, String... values) {
		getMsg(event, labelKey)
		.ifPresent(msg -> errorEmbed(event, replaceValues(msg, values)));
	}
	
	public static boolean exHandling(MessageReceivedEvent event, Throwable ex, String tag) {
		
		if(ex == null || ex.getCause() == null) return false;
		
		// 400
		if(ex.getCause() instanceof BadRequestException) {
			badRequest(event);
			logger.atError().log(ex.toString());
			return true;
		// 403
		}else if(ex.getCause() instanceof AuthenticationException) {
			authError(event);
			logger.atError().log(ex.toString());
			return true;
		// 404
		}else if(ex.getCause() instanceof NotFoundException) {
			tagNotFound(event, tag);
			logger.atError().log("Tag player errato: {}", tag);
			return true;
		// 429
		}else if(ex.getCause() instanceof RateLimitExceededException) {
			rateLimit(event);
			logger.atError().log(ex.toString());
			return true;
		// 503
		}else if(ex.getCause() instanceof MaintenanceException) {
			maintenance(event);
			logger.atError().log(ex.toString());
			return true;
		}else if(ex.getCause() instanceof HttpConnectTimeoutException) {
			timeout(event);
			logger.atError().log(ex.toString());
			return true;
		}else{
			unknownError(event);
			logger.atError().log(ex.toString());
			return true;
		}
	}

	private static void tagNotFound(MessageReceivedEvent event, String tag) {
		getMsg(event, GENERIC_TAG_NOTFOUND)
		.ifPresent(msg -> errorEmbed(event, replaceValues(msg, tag)));
	}
	
	private static void badRequest(MessageReceivedEvent event) {
		getMsg(event, GENERIC_APP_BADREQUEST)
		.ifPresent(msg -> errorEmbed(event, msg));
	}
	
	private static void authError(MessageReceivedEvent event) {
		getMsg(event, GENERIC_APP_AUTHERROR)
		.ifPresent(msg -> errorEmbed(event, msg));
	}
	
	private static void rateLimit(MessageReceivedEvent event) {
		getMsg(event, GENERIC_APP_RATELIMIT)
		.ifPresent(msg -> warningEmbed(event, msg));
	}
	
	private static void maintenance(MessageReceivedEvent event) {
		getMsg(event, GENERIC_APP_MAINTENANCE)
		.ifPresent(msg -> warningEmbed(event, msg));
	}
	
	private static void unknownError(MessageReceivedEvent event) {
		getMsg(event, GENERIC_APP_TIMEOUT)
		.ifPresent(msg -> errorEmbed(event, msg));
	}
	
	private static void timeout(MessageReceivedEvent event) {
		getMsg(event, GENERIC_APP_TIMEOUT)
		.ifPresent(msg -> errorEmbed(event, msg));
	}
	
}