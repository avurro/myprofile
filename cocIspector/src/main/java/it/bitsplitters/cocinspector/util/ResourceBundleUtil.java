package it.bitsplitters.cocinspector.util;

import static it.bitsplitters.cocinspector.Config.RESOURCE_NAME;
import static it.bitsplitters.cocinspector.Config.getSettings;
import static it.bitsplitters.util.ResourceBundleUtil.customControl;
import static java.util.ResourceBundle.getBundle;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.ResourceBundle;

import it.bitsplitters.setting.Settings;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import static it.bitsplitters.cocinspector.util.MessageUtil.*;
public class ResourceBundleUtil {

	public static final String ROLE_RESERVED = "role.reserved";
	
	public static final String INFO = "cmd.help.info";
	
	public static final String RENAME = "cmd.help.rename";
	public static final String RENAME_OK = "cmd.ok.rename";
	public static final String RENAME_PARAMETER_INVALID = "cmd.msg.rename.parameterinvalid";
	public static final String RENAME_WRONGPARAM ="cmd.msg.rename.wrongparam";
	public static final String RENAME_MISSING_PERMISSIONS = "cmd.msg.rename.missingpermissions";
	public static final String RENAME_UNKNOWN_MEMBER = "cmd.msg.rename.unknownmember";
	
	public static final String VIEW = "cmd.help.view";
	public static final String VIEW_NOACCOUNTS = "cmd.msg.view.noaccounts";
	
	public static final String VIEW_PLUS = "cmd.help.viewplus";
	public static final String VIEW_PLUS_NOACCOUNTS = "cmd.msg.viewplus.noaccounts";
	
	
	public static final String UPDATE = "cmd.help.update"; 
	public static final String UPDATE_UPTODATE = "cmd.ok.update.uptodate"; 
	public static final String UPDATE_UPDATED = "cmd.ok.update.updated"; 
	
	public static final String UNLINK = "cmd.help.unlink"; 
	public static final String UNLINK_OK = "cmd.ok.unlink"; 
	public static final String UNLINK_KO = "cmd.ko.unlink"; 
	
	public static final String UNLINK_NOTLINKED = "cmd.msg.unlink.notlinked";
	public static final String UNLINK_NOTLINKED_ANYONE = "cmd.msg.unlink.notlinkedanyone";
	
	public static final String LINK = "cmd.help.link";
	public static final String LINK_OK ="cmd.ok.link";
	public static final String LINK_KO ="cmd.ko.link";
	public static final String LINK_LINKED = "cmd.msg.link.alreadylinked";
	public static final String LINK_LINKED_OTHERS = "cmd.msg.link.linkedtosomeone";
	public static final String LINK_INVALID_API = "cmd.msg.link.invalidapikey";
	
	public static final String GENERIC_SUB_INVALID = "control.guild.notvalidpayer";
	public static final String GENERIC_TAG_NOTFOUND = "control.tag.notfound";
	
	public static final String GENERIC_PROFILE_NOTFOUND = "control.profile.notfound";
	public static final String GENERIC_USER_NOTMENTIONED = "control.user.notmentioned";
	public static final String GENERIC_APP_NOTFOUND = "control.app.notfound";
	public static final String GENERIC_APP_BADREQUEST ="control.app.badrequest";  
	public static final String GENERIC_APP_AUTHERROR ="control.app.autherror";   
	public static final String GENERIC_APP_RATELIMIT ="control.app.ratelimit";  	
	public static final String GENERIC_APP_MAINTENANCE ="control.app.maintenance";
	public static final String GENERIC_APP_UNKNOWNERROR ="control.app.unknown";
	public static final String GENERIC_APP_TIMEOUT ="control.app.timeout";
	public static final String GENERIC_APP_CANTINTERACT ="control.app.cantinteract";
	
	public static final String USER_LANG = "cmd.help.language";
	public static final String USER_LANG_OK = "cmd.ok.language";
	public static final String USER_LANG_NOTEXIST = "cmd.msg.language.error";
	
	public static final String USER_PREFIX = "cmd.help.prefix";
	public static final String USER_PREFIX_OK = "cmd.ok.prefix";
	public static final String USER_PREFIX_LENGTH = "cmd.msg.prefix.length";
	
	public static final String GUILDS_ROLE = "cmd.help.guildsrole";
	public static final String GUILDS_ROLE_MULTY = "cmd.msg.guildrole.many";
	public static final String GUILDS_ROLE_NOTEXIST = "cmd.msg.guildrole.notexist";
	public static final String GUILDS_ROLE_OK = "cmd.ok.guildrole";
	
	public static final String GUILDS_LANG = "cmd.help.guildslanguage";
	public static final String GUILDS_LANG_OK = "cmd.ok.guildslanguage";
	public static final String GUILDS_LANG_NOTEXIST = "cmd.msg.guildslanguage.error";
	
	public static final String GUILDS_PREFIX = "cmd.help.guildsprefix";
	public static final String GUILDS_PREFIX_OK = "cmd.ok.guildsprefix";
	public static final String GUILDS_PREFIX_LENGTH ="cmd.msg.guildsprefix.length";

	public static final String GUILDS_STATUS = "cmd.help.guildsstatus";
	public static final String GUILDS_STATUS_OK = "cmd.ok.guildsstatus";
	public static final String GUILDS_STATUS_NOTEXIST = "cmd.msg.guildsstatus.notexist";
	public static final String GUILDS_STATUS_NOTACTIVE = "cmd.msg.guildsstatus.notactive";

	public static final String GUILDS_MONTHS = "cmd.help.guildsmonths";
	public static final String GUILDS_MONTHS_OK = "cmd.ok.guildsmonths";
	public static final String GUILDS_DATA_OK = "cmd.ok.guildsdata";
	
	public static final String GUILDS_NEW = "cmd.help.guildsnew";
	public static final String GUILDS_NEW_NOTEXIST = "cmd.msg.guildsnew.wrongid";
	public static final String GUILDS_NEW_OK = "cmd.ok.guildsnew";

	public static final String GUILDS_LIST = "cmd.help.guildslist";
	
	public static final String BOT_MNT = "cmd.help.bot.maintenance";
	public static final String BOT_MNT_OK = "cmd.ok.bot.maintenance";
	
	private static Map<Locale, ResourceBundle> guildsRB;
	public static Map<String, String> systemVariables;

	static {
		guildsRB = new HashMap<>();
		systemVariables = new HashMap<>();
		
		systemVariables.put("$app", "CoC Inspector");
		systemVariables.put("$cmd", CMD_EMOJI);
		systemVariables.put("$cmdsub", CMD_SUB_EMOJI);
		
	}
	
	
	public String getMsg(MessageReceivedEvent event, String key) {
		Optional<? extends Settings> optSettings = getSettings(event);
		
		if(!optSettings.isPresent()) return "";
		
		return getResourceBundle(optSettings.get().locale()).getString(key);
			
	}
	
	public static ResourceBundle getResourceBundle(Locale locale) {
		
		if(guildsRB.containsKey(locale))
			return guildsRB.get(locale);
		
		ResourceBundle bundle = getBundle(RESOURCE_NAME, locale, customControl);
		
		guildsRB.put(locale, bundle);
		
		return bundle;
	}
	
	public static String replaceValues(String msg, String...values) {
		
		String result =  it.bitsplitters.util.ResourceBundleUtil.replaceValues(msg, values);
		for (Entry<String, String> variable : systemVariables.entrySet())
			result = result.replace(variable.getKey(), variable.getValue());

		return result;
	}
}
