package it.bitsplitters.cocinspector;


import static it.bitsplitters.cocinspector.Config.Languages.ENG;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.GENERIC_SUB_INVALID;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.getResourceBundle;
import static it.bitsplitters.util.BotUtil.unauthorizedEmbed;

import java.awt.Color;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import it.bitsplitters.cocinspector.commands.CmdInfo;
import it.bitsplitters.cocinspector.commands.botadmin.CmdBotAdminMaintenance;
import it.bitsplitters.cocinspector.commands.botadmin.CmdBotAdminSubExtend;
import it.bitsplitters.cocinspector.commands.botadmin.CmdBotAdminSubList;
import it.bitsplitters.cocinspector.commands.botadmin.CmdBotAdminSubNew;
import it.bitsplitters.cocinspector.commands.guilds.CmdGuildLanguage;
import it.bitsplitters.cocinspector.commands.guilds.CmdGuildPrefix;
import it.bitsplitters.cocinspector.commands.guilds.CmdGuildStatus;
import it.bitsplitters.cocinspector.commands.users.CmdLanguage;
import it.bitsplitters.cocinspector.commands.users.CmdLink;
import it.bitsplitters.cocinspector.commands.users.CmdPrefix;
import it.bitsplitters.cocinspector.commands.users.CmdRename;
import it.bitsplitters.cocinspector.commands.users.CmdUnlink;
import it.bitsplitters.cocinspector.commands.users.CmdUpdate;
import it.bitsplitters.cocinspector.commands.users.CmdView;
import it.bitsplitters.cocinspector.commands.users.CmdViewPlus;
import it.bitsplitters.cocinspector.jooq.models.tables.records.GuildsRecord;
import it.bitsplitters.cocinspector.util.MessageUtil;
import it.bitsplitters.command.GeneralCommand;
import it.bitsplitters.setting.BotConfig;
import it.bitsplitters.setting.Settings;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
/**
 * Where all configurations reside.
 * @author AVurro
 *
 */
public class Config {
	
	private static final Logger logger = LogManager.getLogger();
	
	/** where the configurations reside */
	private static final Properties configProperties = new Properties();
	
	/** Discord token, used to communicate with Discord */
	private static final String botToken = "bot.token";
	
	/** CoC Token, used to interact with Clash of Clans API */
	private static final String cocAPI = "coc.api";
	
	/** DB username */
	private static final String dbUsername = "db.username";
	
	/** DB password */
	private static final String dbPassword = "db.password";
	
	/** DB url */
	private static final String dbUrl = "db.url";

	/** JOOQ context, used to interact with DB */
	private static DSLContext context;
    
	/** JDA */
	private static JDA jda;
	
	/** 
	 * DEFAULT SETTINGS
	 * */
	public static Byte   appID = 0; // CoC Ispector
	public static String defaultPrefix = "-";
	public static String locale = ENG.get();
	public static Color  color = new Color(255,165,0);
	
	public static final String datePattern = "dd-MM-yy";
	
	/** Server Discord configuration, how the bot interacts with the server */
	public static final BotConfig botConfig;

	/** Manager that verifies if a specific user can use the command invoked */
	public static final BotCommandsManager commandsManager;
	
	/** Guilds Manager */
	public static final GuildsManager guildsManager;
	
	/** Users Manager */
	public static final UsersManager usersManager;
	
	/** Bot Commands, the core of the bot */
	public static final GeneralCommand[] commands;
	
	/** resource bundle name */
	public static final String RESOURCE_NAME = "resource";
	
	/** BOT Admins */
	private static final long ALEX = 198341984346308609L;
	
	public static final Map<String, List<Class<?>>> commandsPagination;
	
	static {
    	
    	commands = new GeneralCommand[] {
    			
    			// PUBLIC
    			new CmdLink(),
    			new CmdUnlink(),
    			new CmdUpdate(),
    			new CmdView(),
    			new CmdViewPlus(),
    			new CmdRename(),
    			new CmdLanguage(),
    			new CmdPrefix(),
    			
    			// GUILD
    			new CmdGuildLanguage(),
    			new CmdGuildPrefix(),
    			new CmdGuildStatus(),
    			
    			//BOT ADMINS
    			new CmdBotAdminSubExtend(),
    			new CmdBotAdminSubNew(),
    			new CmdBotAdminSubList(),
    			new CmdBotAdminMaintenance(),
    			
    			//INFO
    			new CmdInfo()};
    	
    	commandsPagination = new LinkedHashMap<>();
    	
    	commandsPagination.put("DM Bot commands|<:cocInspector:821156215907156009>|255,165,0", 
    			List.of(CmdLink.class,
    					CmdUnlink.class,
    					CmdLanguage.class,
    					CmdPrefix.class));
    	
    	commandsPagination.put("Public commands|<:public:821076867016687699>|255,165,0", 
				List.of(CmdView.class,
						CmdViewPlus.class,
				        CmdUpdate.class,
				        CmdRename.class));
    	
    	commandsPagination.put("Guild settings|<:setting:821076866948923404>|255,165,0", 
			    List.of(CmdGuildLanguage.class, 
					    CmdGuildPrefix.class,
		                CmdGuildStatus.class));
    	
    	commandsPagination.put("Information|<:info:821076867238330388>|255,165,0", List.of(CmdInfo.class));
    	
    	commandsManager = new BotCommandsManager();
    	guildsManager = new GuildsManager();
    	usersManager = new UsersManager();
    	
    	botConfig = new BotConfig() {
    		@Override public boolean isCaseSensitive() { return false; }
    		@Override public List<Long> adminIDs() { return Arrays.asList(ALEX); }
    		@Override public String resourceBundle() { return RESOURCE_NAME; }
    	};
    	
		try(FileInputStream file = new FileInputStream("./cocispector.properties")) {
		    configProperties.load(file);

			BasicDataSource ds = new BasicDataSource();
			ds.setUrl(configProperties.getProperty(dbUrl));
			ds.setDriverClassName("org.mariadb.jdbc.Driver");
			ds.setUsername(configProperties.getProperty(dbUsername));
			ds.setPassword(configProperties.getProperty(dbPassword));
			ds.setMinIdle(5);
			ds.setMaxIdle(10);
			ds.setMaxOpenPreparedStatements(10);
			context = DSL.using(ds, SQLDialect.MYSQL);
			
		} catch (IOException e) { logger.atError().log(e.getMessage()); }
    }
 
	public static boolean isValidPayer(MessageReceivedEvent event) {
		GuildsRecord guild = guildsManager.getGuild(event.getGuild().getIdLong());
		
		if(!guildsManager.isValidPayer(guild.getIdGuild().longValue())) {
			String msg = getResourceBundle(Locale.forLanguageTag(guild.getLocale())).getString(GENERIC_SUB_INVALID);
			unauthorizedEmbed(event, msg);
			return false;
		}
		
		return true;
	}

	public static Optional<String> getMsg(MessageReceivedEvent event, String labelKey) {
		Optional<? extends Settings> settings = getSettings(event);
		if(!settings.isPresent()) return Optional.empty();
		return Optional.of(getMsg(settings.get(), labelKey));
	}
	
	public static String getMsg(Settings settings, String labelKey) {
		return getResourceBundle(settings.locale()).getString(labelKey);
	}

	public static String getMsg(GuildsRecord guild, String labelKey) {
		return getResourceBundle(Locale.forLanguageTag(guild.getLocale())).getString(labelKey);
	}

	public static Optional<? extends Settings> getSettings(MessageReceivedEvent event){
		return event.getChannel().getType().isGuild() 
			    ? guildsManager.find(event.getGuild().getIdLong())
			    : usersManager.find(event.getAuthor().getIdLong());
	}
	
    public static DSLContext jooq() {
    	return context;
    }
	
    public static void setJDA(JDA jda) {
    	Config.jda = jda;
    	MessageUtil.setCocInspectorEmote();
    }
    
    public static JDA JDA() {
    	return Config.jda;
    }
    
	public static String getBotToken() {
		return configProperties.getProperty(botToken);
	}

	public static String getCocApi() {
		return configProperties.getProperty(cocAPI);
	}

	public static enum Languages { 
		
		ENG(Locale.ENGLISH), ITA(Locale.ITALY);  

		private Locale locale;
		
		private Languages(Locale locale) { this.locale = locale; }
		public String get () { return this.locale.getLanguage(); }
		public Locale locale() { return this.locale; }
		
		public static List<String> labels = Arrays.asList("it","en");
	}

	private Config() {}
}
