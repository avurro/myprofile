package it.bitsplitters.cocinspector;

public enum PayLevel {
	FREE((byte)0,"Free use"),
	SMALL_SERVER((byte)1,"Small Server"),
	MEDIUM_SERVER((byte)2,"Medium Server"),
	LARGE_SERVER((byte)3,"Large Server"); 
	
	private byte level;
	private String description;

	private PayLevel(byte level, String description) {
		this.level = level;
		this.description = description;
	}
	
	public static boolean isFREE(byte level) {
		return FREE.level == level;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public byte getLevel() {
		return level;
	};
}
