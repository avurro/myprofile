package it.bitsplitters.cocinspector.adapters;

import java.util.Locale;

import it.bitsplitters.cocinspector.jooq.models.tables.records.GuildsRecord;
import it.bitsplitters.cocinspector.jooq.models.tables.records.ProfileRecord;
import it.bitsplitters.cocinspector.model.MyProfileGuild;
import it.bitsplitters.cocinspector.model.MyProfileUser;
public class Adapters {

	public static MyProfileGuild toGuild(GuildsRecord record) {
		return new MyProfileGuild(record.getIdGuild().longValue(), record.getPrefix(), Locale.forLanguageTag(record.getLocale()));
	}
	
	public static MyProfileUser toUser(ProfileRecord record) {
		return new MyProfileUser(record.getIdProfile().longValue(), record.getPrefix(), Locale.forLanguageTag(record.getLocale()));
	}
}
