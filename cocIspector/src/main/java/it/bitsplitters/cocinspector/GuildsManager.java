package it.bitsplitters.cocinspector;

import static it.bitsplitters.cocinspector.Config.appID;
import static it.bitsplitters.cocinspector.Config.defaultPrefix;
import static it.bitsplitters.cocinspector.Config.jooq;
import static it.bitsplitters.cocinspector.Config.locale;
import static it.bitsplitters.cocinspector.adapters.Adapters.toGuild;
import static it.bitsplitters.cocinspector.jooq.models.Tables.GUILDS;

import java.time.LocalDate;
import java.util.Optional;

import org.jooq.types.ULong;

import it.bitsplitters.cocinspector.jooq.models.tables.records.GuildsRecord;
import it.bitsplitters.setting.GuildSettings;

public class GuildsManager implements it.bitsplitters.GuildsManager {

	@Override
	public Optional<GuildSettings> find(Long guildID) {
		
		Optional<GuildsRecord> optGuildsRecord = findRecord(guildID);
		
		return optGuildsRecord.isEmpty()
				? Optional.empty()
				: Optional.of(toGuild(optGuildsRecord.get()));
	}

	@Override
	public void guildJoin(Long guildID) {
		
		if(find(guildID).isEmpty()) 
			register(guildID);
		
	}

	@Override
	public void guildLeave(Long guildID) {
				
		GuildsRecord guild = findRecord(guildID).get();
				
		if(PayLevel.isFREE(guild.getPayLevel()))
			jooq().executeDelete(guild);
	}
	
	public boolean isValidPayer(Long guildID) {
		return jooq().selectFrom(GUILDS)
        .where(GUILDS.ID_GUILD.eq(ULong.valueOf(guildID))
        .and(GUILDS.PAY_END.greaterOrEqual(LocalDate.now())))
        .and(GUILDS.ID_APP.eq(Config.appID))
		.fetchOptional()
        .isPresent();
	}
	
	public GuildsRecord getGuild(Long guildID) {
		
		return findRecord(guildID).get();
		
	}
	
	public Optional<GuildsRecord> findRecord(Long guildID) {
		return jooq().selectFrom(GUILDS)
					.where(GUILDS.ID_GUILD.eq(ULong.valueOf(guildID))
					.and(GUILDS.ID_APP.eq(Config.appID)))
					.fetchOptional();
	}

	@Override
	public GuildSettings register(Long guildID) {
		ULong id = ULong.valueOf(guildID);
		byte payLevel = PayLevel.FREE.getLevel();
		LocalDate payEnd = null;
		
		GuildsRecord guild = new GuildsRecord(id, appID, payLevel, payEnd, defaultPrefix, locale, null);
		jooq().executeInsert(guild);
		return toGuild(guild);
	}
}