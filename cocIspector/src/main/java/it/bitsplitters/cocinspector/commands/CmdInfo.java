package it.bitsplitters.cocinspector.commands;

import static it.bitsplitters.cocinspector.util.MessageUtil.infoMessage;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.INFO;

import it.bitsplitters.command.ChannelEnabled;
import it.bitsplitters.command.signature.Signature;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class CmdInfo extends CoCIspectorCommand {

	@Override public String name() { return "info"; }

	@Override public String helpKey() { return INFO; }

	@Override public ChannelEnabled channelEnabled() {
		return ChannelEnabled.ALL;
	}
	
	@Signature
	public void info(MessageReceivedEvent event) {
		infoMessage(event, INFO);
	}
}