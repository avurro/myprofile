package it.bitsplitters.cocinspector.commands.users;

import static it.bitsplitters.cocinspector.Config.usersManager;
import static it.bitsplitters.cocinspector.services.AccountService.findAPIAccount;
import static it.bitsplitters.cocinspector.services.AccountService.getDBAccount;
import static it.bitsplitters.cocinspector.util.MessageUtil.errorMessage;
import static it.bitsplitters.cocinspector.util.MessageUtil.okMessage;
import static it.bitsplitters.cocinspector.util.MessageUtil.unauthorizedMessage;
import static it.bitsplitters.cocinspector.util.MessageUtil.warningMessage;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.GENERIC_APP_CANTINTERACT;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.RENAME;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.RENAME_MISSING_PERMISSIONS;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.RENAME_OK;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.RENAME_UNKNOWN_MEMBER;
import static it.bitsplitters.util.BotUtil.errorEmbed;

import it.bitsplitters.cocinspector.commands.CoCIspectorCommand;
import it.bitsplitters.command.ChannelEnabled;
import it.bitsplitters.command.signature.Signature;
import it.bitsplitters.command.signature.parameter.impl.StringParameter;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.exceptions.ErrorResponseException;

public class CmdRename extends CoCIspectorCommand {

	
	@Override public String name() { return "rename"; }

	@Override public String helpKey() { return RENAME; }

	@Override public ChannelEnabled channelEnabled() {
		return ChannelEnabled.GUILD;
	}

	@Signature
	public void rename(MessageReceivedEvent event) {
		rename(event, "1");
	}
	
	@Signature(order = 1, parameters = {StringParameter.class})
	public void rename(MessageReceivedEvent event, String tagOrPosition) {

		if(checkMaintenance(event))
			return;
		
		usersManager.ifAbsentRegisterIt(event.getAuthor().getIdLong());
		
		getDBAccount(event, tagOrPosition).ifPresent((accountToRename) -> {
			
			findAPIAccount(event, accountToRename.getTag(), (e, player) -> {
				
				if(!event.getGuild().getSelfMember().canInteract(event.getMember())) {
					warningMessage(event, GENERIC_APP_CANTINTERACT);
					return;
				}
				
				event.getMember().modifyNickname(player.getName()+" | "+player.getClan().getName())
				.queue(
						
					(success) ->  okMessage(event, RENAME_OK),
					
					(renameEx) -> {
						
						if(renameEx instanceof ErrorResponseException) 
							switch(((ErrorResponseException)renameEx).getErrorResponse()) {
							
							case MISSING_PERMISSIONS:		
								unauthorizedMessage(event, RENAME_MISSING_PERMISSIONS);
								break;
								
							case UNKNOWN_MEMBER:			
								errorMessage(event, RENAME_UNKNOWN_MEMBER);
								break;	
								
							default:
								errorEmbed(event, renameEx.toString());
								break;
							}
						
					}
				);
			});
		});
	}
}