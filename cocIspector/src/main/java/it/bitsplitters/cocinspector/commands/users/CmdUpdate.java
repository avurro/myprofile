package it.bitsplitters.cocinspector.commands.users;

import static it.bitsplitters.cocinspector.services.AccountService.findAPIAccount;
import static it.bitsplitters.cocinspector.services.AccountService.getDBAccount;
import static it.bitsplitters.cocinspector.util.MessageUtil.okMessage;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.UPDATE;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.UPDATE_UPDATED;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.UPDATE_UPTODATE;

import it.bitsplitters.cocinspector.commands.CoCIspectorCommand;
import it.bitsplitters.command.ChannelEnabled;
import it.bitsplitters.command.signature.Signature;
import it.bitsplitters.command.signature.parameter.impl.StringParameter;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class CmdUpdate extends CoCIspectorCommand {

	
	@Override public String name() { return "update"; }

	@Override public String helpKey() { return UPDATE; }

	@Override public ChannelEnabled channelEnabled() {
		return ChannelEnabled.ALL;
	}

	@Signature
	public void update(MessageReceivedEvent event) {
		update(event, "1");
	}
	
	@Signature(order = 1, parameters = {StringParameter.class})
	public void update(MessageReceivedEvent event, String tagOrPosition) {

		if(checkMaintenance(event))
			return;
		
		getDBAccount(event, tagOrPosition)
		.ifPresent((dbAccount) -> findAPIAccount(event, dbAccount, (e, dbPlayer, apiPlayer) -> {
			
			if(apiPlayer.getName().equals(dbPlayer.getNickname()) && apiPlayer.getTownHallLevel() == dbPlayer.getThLevel()) 
				okMessage(event, UPDATE_UPTODATE);
				
			else {
				dbPlayer.setNickname(apiPlayer.getName());
				dbPlayer.setThLevel((byte) apiPlayer.getTownHallLevel());
				dbPlayer.store();
				
				okMessage(e, UPDATE_UPDATED);
			}
	    }));
	}
}