package it.bitsplitters.cocinspector.commands.botadmin;

import it.bitsplitters.cocinspector.commands.CoCIspectorCommand;
import it.bitsplitters.command.PermissionLevel;

public abstract class BotAdminCommand extends CoCIspectorCommand {

	@Override public PermissionLevel permissionLevel() {
		return PermissionLevel.BOT_ADMIN;
	}
	
}