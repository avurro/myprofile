package it.bitsplitters.cocinspector.commands.botadmin;

import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.*;
import static it.bitsplitters.cocinspector.util.MessageUtil.okMessage;
import it.bitsplitters.cocinspector.commands.CoCIspectorCommand;
import it.bitsplitters.command.signature.Signature;
import it.bitsplitters.command.signature.parameter.impl.BooleanParameter;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class CmdBotAdminMaintenance extends BotAdminCommand {

	@Override public String name() {
		return "bot mnt";
	}

	@Override public String helpKey() {
		return BOT_MNT;
	}

	@Signature(parameters = {BooleanParameter.class, BooleanParameter.class})
	public void command(MessageReceivedEvent event, Boolean maintenance, Boolean writeMessage) {
		
		CoCIspectorCommand.isMaintenance = maintenance;
		CoCIspectorCommand.maintenanceMessage = writeMessage;
		
		okMessage(event, BOT_MNT_OK, maintenance.toString(), writeMessage.toString());
	}
}