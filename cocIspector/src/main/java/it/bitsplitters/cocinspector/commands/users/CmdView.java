package it.bitsplitters.cocinspector.commands.users;

import static it.bitsplitters.cocinspector.Config.usersManager;
import static it.bitsplitters.cocinspector.services.AccountService.getAccounts;
import static it.bitsplitters.cocinspector.util.MessageUtil.getPlayerLink;
import static it.bitsplitters.cocinspector.util.MessageUtil.getTHemoji;
import static it.bitsplitters.cocinspector.util.MessageUtil.warningMessage;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.VIEW;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.VIEW_NOACCOUNTS;
import static it.bitsplitters.util.BotUtil.printEmbed;

import java.util.StringJoiner;

import org.jooq.Result;

import it.bitsplitters.cocinspector.Config;
import it.bitsplitters.cocinspector.commands.CoCIspectorCommand;
import it.bitsplitters.cocinspector.jooq.models.tables.records.CocRecord;
import it.bitsplitters.command.ChannelEnabled;
import it.bitsplitters.command.signature.Signature;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class CmdView extends CoCIspectorCommand {

	
	@Override public String name() { return "view"; }

	@Override public String helpKey() { return VIEW; }

	@Override public ChannelEnabled channelEnabled() {
		return ChannelEnabled.ALL;
	}

	@Signature
	public void view(MessageReceivedEvent event) {

		if(checkMaintenance(event))
			return;
		
		Result<CocRecord> accounts = getAccounts(usersManager.ifAbsentRegisterIt(event.getAuthor().getIdLong()));
			
		if(accounts.isEmpty()) {
			warningMessage(event, VIEW_NOACCOUNTS);
			return;
		}
		
		int pos = 1;
		StringJoiner detail = new StringJoiner("\n");
		for (CocRecord account : accounts) 
			detail.add(String.join("","***",pos++ +".*** ",getTHemoji(account.getThLevel())," ",getPlayerLink(account.getNickname(), account.getTag())));
		
		printEmbed(event, detail.toString(), event.getAuthor().getName()+ " accounts", Config.color);
	}
}