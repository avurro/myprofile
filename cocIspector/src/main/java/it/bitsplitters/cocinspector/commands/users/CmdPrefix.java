package it.bitsplitters.cocinspector.commands.users;

import static it.bitsplitters.cocinspector.Config.usersManager;
import static it.bitsplitters.cocinspector.jooq.models.tables.Profile.PROFILE;
import static it.bitsplitters.cocinspector.util.MessageUtil.okMessage;
import static it.bitsplitters.cocinspector.util.MessageUtil.warningMessage;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.USER_PREFIX;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.USER_PREFIX_LENGTH;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.USER_PREFIX_OK;

import it.bitsplitters.cocinspector.commands.CoCIspectorCommand;
import it.bitsplitters.cocinspector.jooq.models.tables.records.ProfileRecord;
import it.bitsplitters.command.ChannelEnabled;
import it.bitsplitters.command.PermissionLevel;
import it.bitsplitters.command.signature.Signature;
import it.bitsplitters.command.signature.parameter.impl.StringParameter;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class CmdPrefix extends CoCIspectorCommand {

	@Override public PermissionLevel permissionLevel() {
		return PermissionLevel.PUBLIC;
	}
	
	@Override public String name() {
		return "prefix";
	}

	@Override public String helpKey() {
		return USER_PREFIX;
	}
	
	@Override public ChannelEnabled channelEnabled() {
		return ChannelEnabled.DMBOT;
	}
	
	@Signature(parameters = StringParameter.class)
	public void command(MessageReceivedEvent event, String prefix) {

		if(checkMaintenance(event))
			return;
		
		ProfileRecord profile = usersManager.ifAbsentRegisterIt(event.getAuthor().getIdLong());
		
		if(prefix.length() > PROFILE.PREFIX.getDataType().length()) {
			warningMessage(event, USER_PREFIX_LENGTH);
			return;	
		}
		
		profile.setPrefix(prefix);
		profile.store();
		
		okMessage(event, USER_PREFIX_OK);
	}

}
