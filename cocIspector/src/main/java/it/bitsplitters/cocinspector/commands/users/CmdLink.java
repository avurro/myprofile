package it.bitsplitters.cocinspector.commands.users;

import static it.bitsplitters.cocinspector.Config.jooq;
import static it.bitsplitters.cocinspector.Config.usersManager;
import static it.bitsplitters.cocinspector.jooq.models.tables.Coc.COC;
import static it.bitsplitters.cocinspector.jooq.models.tables.Profile.PROFILE;
import static it.bitsplitters.cocinspector.util.MessageUtil.errorMessage;
import static it.bitsplitters.cocinspector.util.MessageUtil.exHandling;
import static it.bitsplitters.cocinspector.util.MessageUtil.getPlayerLink;
import static it.bitsplitters.cocinspector.util.MessageUtil.okMessage;
import static it.bitsplitters.cocinspector.util.MessageUtil.warningMessage;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.LINK;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.LINK_INVALID_API;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.LINK_KO;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.LINK_LINKED;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.LINK_LINKED_OTHERS;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.LINK_OK;
import static it.bitsplitters.cocinspector.services.AccountService.*;
import java.util.Optional;

import org.jooq.Result;
import org.jooq.types.ULong;

import it.bitsplitters.clashapi.Player;
import it.bitsplitters.cocinspector.API;
import it.bitsplitters.cocinspector.commands.CoCIspectorCommand;
import it.bitsplitters.cocinspector.jooq.models.tables.records.CocRecord;
import it.bitsplitters.cocinspector.jooq.models.tables.records.ProfileRecord;
import it.bitsplitters.command.ChannelEnabled;
import it.bitsplitters.command.signature.Signature;
import it.bitsplitters.command.signature.parameter.impl.StringParameter;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class CmdLink extends CoCIspectorCommand {

	
	@Override public String name() { return "link"; }

	@Override public String helpKey() { return LINK; }

	@Override public ChannelEnabled channelEnabled() {
		return ChannelEnabled.DMBOT;
	}
	
	@Signature(parameters = {StringParameter.class, StringParameter.class})
	public void link(MessageReceivedEvent event, String tag, String token) {

		if(checkMaintenance(event))
			return;
		
		long userID = event.getAuthor().getIdLong();
		
		ProfileRecord profile = usersManager.ifAbsentRegisterIt(userID);
		
		Optional<CocRecord> linkedAccount = getLinkedAccount(userID, tag);
		if(linkedAccount.isPresent()) {
			warningMessage(event, LINK_LINKED, getPlayerLink(linkedAccount.get().getNickname(), tag));
			return;
		}
		
		linkedAccount = getLinkedAccountToSomeone(tag);
		if(linkedAccount.isPresent()) {
			warningMessage(event, LINK_LINKED_OTHERS, getPlayerLink(linkedAccount.get().getNickname(), tag));
			return;
		}
		
		findAPIAccount(event, tag, (e, player) -> {
			
			API.COC.verifyToken(tag, token)
			.whenComplete((playerToken, tokenEx) -> {
				
				if(exHandling(event, tokenEx, tag))
					return;
				
				switch(playerToken.getStatus().toUpperCase()) {
				
				case "INVALID": 
					warningMessage(event, LINK_INVALID_API);
					break;
					
				case "OK": 
					if(insertTag(profile, player) >= 1) okMessage(event, LINK_OK);
					else 						  errorMessage(event, LINK_KO);
					break;
				}
			});
			
		});
	}
	
	private Optional<CocRecord> getLinkedAccount(Long userID, String tag) {
		
		 CocRecord result = jooq().select(COC.fields())
								.from(PROFILE).naturalJoin(COC)
								.where(PROFILE.ID_DISCORD.eq(ULong.valueOf(userID))
								.and(COC.TAG.eq(tag)))
								.fetchAnyInto(COC);
			   
		 return result == null ? Optional.empty() : Optional.of(result);
	}
	
	private Optional<CocRecord> getLinkedAccountToSomeone(String tag) {
		
		Result<CocRecord> result = jooq().selectFrom(COC).where(COC.TAG.eq(tag)).fetch();
		return result.isEmpty() ? Optional.empty() : Optional.of(result.get(0));

	}
	
	private int insertTag(ProfileRecord user, Player player) {
		return jooq().executeInsert(new CocRecord(user.getIdProfile(), player.getTag(),player.getName(), (byte)player.getTownHallLevel()));
	}
}