package it.bitsplitters.cocinspector.commands.guilds;

import static it.bitsplitters.cocinspector.Config.guildsManager;
import static it.bitsplitters.cocinspector.jooq.models.tables.Guilds.GUILDS;
import static it.bitsplitters.cocinspector.util.MessageUtil.okMessage;
import static it.bitsplitters.cocinspector.util.MessageUtil.warningMessage;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.GUILDS_PREFIX;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.GUILDS_PREFIX_LENGTH;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.GUILDS_PREFIX_OK;

import it.bitsplitters.cocinspector.commands.CoCIspectorCommand;
import it.bitsplitters.cocinspector.jooq.models.tables.records.GuildsRecord;
import it.bitsplitters.command.PermissionLevel;
import it.bitsplitters.command.signature.Signature;
import it.bitsplitters.command.signature.parameter.impl.StringParameter;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class CmdGuildPrefix extends CoCIspectorCommand {

	@Override public PermissionLevel permissionLevel() {
		return PermissionLevel.ADMIN;
	}
	
	@Override public String name() {
		return "guild prefix";
	}

	@Override public String helpKey() {
		return GUILDS_PREFIX;
	}

	@Signature(parameters = StringParameter.class)
	public void command(MessageReceivedEvent event, String prefix) {

		if(checkMaintenance(event))
			return;
		
		if(prefix.length() > GUILDS.PREFIX.getDataType().length()) {
			warningMessage(event, GUILDS_PREFIX_LENGTH);
			return;	
		}
		
		GuildsRecord guild = guildsManager.getGuild(event.getGuild().getIdLong());
			
		guild.setPrefix(prefix);
		guild.store();
			
		okMessage(event, GUILDS_PREFIX_OK);
	}
}