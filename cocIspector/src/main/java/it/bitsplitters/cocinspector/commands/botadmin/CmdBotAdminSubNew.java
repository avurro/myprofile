package it.bitsplitters.cocinspector.commands.botadmin;

import static it.bitsplitters.cocinspector.Config.datePattern;
import static it.bitsplitters.cocinspector.Config.guildsManager;
import static it.bitsplitters.cocinspector.Config.jooq;
import static it.bitsplitters.cocinspector.util.MessageUtil.okMessage;
import static it.bitsplitters.cocinspector.util.MessageUtil.warningMessage;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.GUILDS_NEW;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.GUILDS_NEW_NOTEXIST;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.GUILDS_NEW_OK;
import static it.bitsplitters.util.BotUtil.warningEmbed;
import static java.time.LocalDate.now;
import static java.time.LocalDate.parse;
import static java.time.format.DateTimeFormatter.ofPattern;

import java.time.LocalDate;
import java.util.function.Function;

import it.bitsplitters.command.signature.Signature;
import it.bitsplitters.command.signature.parameter.impl.ByteParameter;
import it.bitsplitters.command.signature.parameter.impl.IntegerParameter;
import it.bitsplitters.command.signature.parameter.impl.LongParameter;
import it.bitsplitters.command.signature.parameter.impl.StringParameter;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class CmdBotAdminSubNew extends BotAdminCommand {
	
	@Override public String name() {
		return "sub";
	}

	@Override public String helpKey() {
		return GUILDS_NEW;
	}
	
	@Signature(order = 1, parameters = {LongParameter.class, ByteParameter.class, StringParameter.class})
	public void command(MessageReceivedEvent event, Long guildID, Byte payLevel, String data) {
		
		updateCustomer(event, guildID, payLevel,
				
				(payEnd) -> parse(data, ofPattern(datePattern))
			
		);
	}
	
	@Signature(parameters = {LongParameter.class, ByteParameter.class, IntegerParameter.class})
	public void command(MessageReceivedEvent event, Long guildID, Byte payLevel, Integer months) {
		
		updateCustomer(event, guildID, payLevel,
				
				(payEnd) -> now().plusMonths(months)
			
		);
	}
	
	private void updateCustomer(MessageReceivedEvent event, Long guildID, Byte payLevel, Function<LocalDate, LocalDate> payEnd) {

		if(checkMaintenance(event))
			return;
		
		guildsManager.findRecord(guildID).ifPresentOrElse(customer -> {
			
			if(payLevel != null)
				customer.setPayLevel(payLevel);
			
			customer.setPayEnd(payEnd.apply(customer.getPayEnd()));
			
			if(jooq().executeUpdate(customer) == 0)
				warningEmbed(event, "Abbonamento non avviato, server non trovato.");
			
			else 
				okMessage(event, GUILDS_NEW_OK);
			
		},
				
		() -> warningMessage(event, GUILDS_NEW_NOTEXIST, guildID.toString()));
	}
}