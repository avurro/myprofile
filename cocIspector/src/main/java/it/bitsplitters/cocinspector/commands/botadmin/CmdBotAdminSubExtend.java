package it.bitsplitters.cocinspector.commands.botadmin;

import static it.bitsplitters.cocinspector.Config.datePattern;
import static it.bitsplitters.cocinspector.Config.guildsManager;
import static it.bitsplitters.cocinspector.Config.jooq;
import static it.bitsplitters.cocinspector.util.MessageUtil.okMessage;
import static it.bitsplitters.cocinspector.util.MessageUtil.warningMessage;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.GUILDS_DATA_OK;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.GUILDS_MONTHS;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.GUILDS_MONTHS_OK;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.GUILDS_NEW_NOTEXIST;
import static it.bitsplitters.util.BotUtil.warningEmbed;
import static java.time.LocalDate.now;
import static java.time.LocalDate.parse;
import static java.time.format.DateTimeFormatter.ofPattern;

import java.time.LocalDate;
import java.util.function.Consumer;
import java.util.function.Function;

import it.bitsplitters.command.signature.Signature;
import it.bitsplitters.command.signature.parameter.impl.IntegerParameter;
import it.bitsplitters.command.signature.parameter.impl.LongParameter;
import it.bitsplitters.command.signature.parameter.impl.StringParameter;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class CmdBotAdminSubExtend extends BotAdminCommand {

	@Override public String name() {
		return "guild extend";
	}

	@Override public String helpKey() {
		return GUILDS_MONTHS;
	}
	
	@Signature(order = 1, parameters = {LongParameter.class, StringParameter.class})
	public void command(MessageReceivedEvent event, Long guildID, String data) {
		
		extend(event, guildID, 
				
			(payEnd) -> parse(data, ofPattern(datePattern)),
			
			(e) -> okMessage(e, GUILDS_DATA_OK, guildID.toString())
			
		);

	}
	
	@Signature(parameters = {LongParameter.class, IntegerParameter.class})
	public void command(MessageReceivedEvent event, Long guildID, Integer months) {

		extend(event, guildID, (payEnd) -> {
			
			if(payEnd == null || payEnd.isBefore(now())) payEnd = now();
			
			return payEnd.plusMonths(months);
			
		},(e) -> okMessage(e, GUILDS_MONTHS_OK, months.toString()));
		
	}
	
	private void extend(MessageReceivedEvent event, Long guildID, Function<LocalDate, LocalDate> payEnd, Consumer<MessageReceivedEvent> okMessage) {
		
		if(checkMaintenance(event))
			return;
		
		guildsManager.findRecord(guildID).ifPresentOrElse(customer -> {
			
			customer.setPayEnd(payEnd.apply(customer.getPayEnd()));
			
			if(jooq().executeUpdate(customer) == 0)
				warningEmbed(event, "Abbonamento non esteso, server non trovato.");
			
			else 
				okMessage.accept(event);
			
		},
				
		() -> warningMessage(event, GUILDS_NEW_NOTEXIST, guildID.toString()));
	}
}