package it.bitsplitters.cocinspector.commands.users;

import static it.bitsplitters.cocinspector.Config.isValidPayer;
import static it.bitsplitters.cocinspector.jooq.models.tables.Coc.COC;
import static it.bitsplitters.cocinspector.services.AccountService.*;
import static it.bitsplitters.cocinspector.util.MessageUtil.getPlayerLink;
import static it.bitsplitters.cocinspector.util.MessageUtil.getTHemoji;
import static it.bitsplitters.cocinspector.util.MessageUtil.warningMessage;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.*;
import static it.bitsplitters.util.BotUtil.printEmbed;

import java.util.List;
import java.util.StringJoiner;

import org.jooq.Record;
import org.jooq.Result;

import it.bitsplitters.cocinspector.Config;
import it.bitsplitters.cocinspector.commands.CoCIspectorCommand;
import it.bitsplitters.cocinspector.jooq.models.tables.records.CocRecord;
import it.bitsplitters.cocinspector.jooq.models.tables.records.ProfileRecord;
import it.bitsplitters.command.ChannelEnabled;
import it.bitsplitters.command.signature.Signature;
import it.bitsplitters.command.signature.parameter.impl.StringParameter;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class CmdViewPlus extends CoCIspectorCommand {

	
	@Override public String name() { return "view+"; }

	@Override public String helpKey() { return VIEW_PLUS; }

	@Override public ChannelEnabled channelEnabled() {
		return ChannelEnabled.GUILD;
	}

	@Signature(parameters = {StringParameter.class})
	public void view(MessageReceivedEvent event, String user) {

		if(checkMaintenance(event))
			return;
		
		if(!isValidPayer(event))
			return;
		
		Result<CocRecord> accounts = null;
		ProfileRecord profile = null;
		
		if(user.startsWith("#")) {
			CocRecord cocAccount = getCocAccount(user);
			
			if(cocAccount == null) {
				warningMessage(event, GENERIC_APP_NOTFOUND, user);
				return;
			}
			
			profile = getProfile(cocAccount.getIdProfile());
			
			if(profile == null) {
				warningMessage(event, GENERIC_PROFILE_NOTFOUND);
				return;
			}
			
			accounts = getAccounts(profile);
			return;
		}
		
		try {
			
			Long discordID = Long.valueOf(user);
			profile = getProfile(discordID);
			
			if(profile == null) {
				warningMessage(event, GENERIC_PROFILE_NOTFOUND);
				return;
			}
			
			accounts = getAccounts(profile);
			
		}catch (NumberFormatException e) {
			List<Member> mentionedMembers = event.getMessage().getMentionedMembers();
			
			if(mentionedMembers.isEmpty()) {
				warningMessage(event, GENERIC_USER_NOTMENTIONED);
				return;
			}
			
			profile = getProfile(mentionedMembers.get(0).getIdLong());
			accounts = getAccounts(profile);
		}
		
		if(accounts.isEmpty()) {
			warningMessage(event, VIEW_PLUS_NOACCOUNTS);
			return;
		}
		
		print(event, profile, accounts);
	}
	
	private void print(MessageReceivedEvent event, ProfileRecord profile, Result<CocRecord> accounts) {
		
		int pos = 1;
		StringJoiner detail = new StringJoiner("\n");
		detail.add("**Discord ID:**").add(profile.getIdDiscord().longValue() + "\n");
		
		for (Record account : accounts) 
			detail.add(String.join("","***",pos++ +".*** ",getTHemoji(account.get(COC.TH_LEVEL))," ",getPlayerLink(account.get(COC.NICKNAME), account.get(COC.TAG))));
		
		printEmbed(event, detail.toString(), "<@&"+profile.getIdDiscord().longValue() + "> accounts", Config.color);
	}
}
