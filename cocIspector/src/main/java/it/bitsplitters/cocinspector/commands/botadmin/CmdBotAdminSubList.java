package it.bitsplitters.cocinspector.commands.botadmin;

import static it.bitsplitters.cocinspector.Config.datePattern;
import static it.bitsplitters.cocinspector.Config.jooq;
import static it.bitsplitters.cocinspector.jooq.models.tables.Guilds.GUILDS;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.GUILDS_LIST;
import static it.bitsplitters.util.BotUtil.infoEmbed;

import java.time.format.DateTimeFormatter;

import org.jooq.Result;
import org.jooq.SelectWhereStep;
import org.jooq.types.ULong;

import it.bitsplitters.cocinspector.jooq.models.tables.records.GuildsRecord;
import it.bitsplitters.command.signature.Signature;
import it.bitsplitters.command.signature.parameter.impl.LongParameter;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class CmdBotAdminSubList extends BotAdminCommand {

	@Override public String name() {
		return "guild list";
	}

	@Override public String helpKey() {
		return GUILDS_LIST;
	}
	
	@Signature
	public void command(MessageReceivedEvent event) {
		command(event, null);
	}
	
	@Signature(order = 1, parameters = LongParameter.class)
	public void command(MessageReceivedEvent event, Long idGuild) {
		
		if(checkMaintenance(event))
			return;
		
		String infoMsg = null;
		
		for(GuildsRecord guild: guilds(idGuild)) {
			
			Guild guildById = event.getJDA().getGuildById(guild.getIdGuild().longValue());
			
			if(guildById == null) 
				infoMsg = "Bot espulso dal server: "+idGuild;
			
			else {
				
				String reserverRole = "Not defined";
				
				if(guild.getIdRoleReserved() != null) 
					reserverRole = guildById.getRoleById(guild.getIdRoleReserved().longValue()).getName();
				
				infoMsg = String.join("",guildById.getName()," (",guildById.getIdLong()+")\n",
						"Pay end:",guild.getPayEnd() != null 
						? guild.getPayEnd().format(DateTimeFormatter.ofPattern(datePattern))
								: "FREE subscription",
								"\nLocale: ", guild.getLocale().toUpperCase(),
								"\nPrefix: ", guild.getPrefix(),
								"\nAssigned roles:", reserverRole);
			}
			
			infoEmbed(event, infoMsg);
				
		}
	}
	
	private Result<GuildsRecord> guilds(Long idGuild){
		
		SelectWhereStep<GuildsRecord> selectGuilds = jooq().selectFrom(GUILDS);
		
		if(idGuild != null)
			selectGuilds.where(GUILDS.ID_GUILD.eq(ULong.valueOf(idGuild)));
		
		return selectGuilds.orderBy(GUILDS.PAY_END.desc()).fetch();
	}
}