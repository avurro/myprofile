package it.bitsplitters.cocinspector.commands.guilds;

import static it.bitsplitters.cocinspector.Config.guildsManager;
import static it.bitsplitters.cocinspector.util.MessageUtil.okMessage;
import static it.bitsplitters.cocinspector.util.MessageUtil.warningMessage;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.GUILDS_ROLE;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.GUILDS_ROLE_MULTY;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.GUILDS_ROLE_NOTEXIST;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.GUILDS_ROLE_OK;

import java.util.List;

import org.jooq.types.ULong;

import it.bitsplitters.cocinspector.commands.CoCIspectorCommand;
import it.bitsplitters.cocinspector.jooq.models.tables.records.GuildsRecord;
import it.bitsplitters.command.PermissionLevel;
import it.bitsplitters.command.signature.Signature;
import it.bitsplitters.command.signature.parameter.impl.StringParameter;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
public class CmdGuildRole extends CoCIspectorCommand {

	@Override public PermissionLevel permissionLevel() {
		return PermissionLevel.ADMIN;
	}
	
	@Override public String name() {
		return "guild role";
	}
	
	@Override public String helpKey() {
		return GUILDS_ROLE;
	}
	
	@Signature(parameters = {StringParameter.class}) 
	public void command(MessageReceivedEvent event, String inputRole) {

		if(checkMaintenance(event))
			return;
		
		GuildsRecord guild = guildsManager.getGuild(event.getGuild().getIdLong());
			
		Role role = null;
		
		try {
			
			role = event.getGuild().getRoleById(inputRole);
			
		}catch (NumberFormatException e) {
			
			String roleName = inputRole;
			if(roleName.startsWith("@"))
				roleName = roleName.substring(1);
			
			List<Role> rolesByName = event.getGuild().getRolesByName(roleName, false);
			
			if(rolesByName.size()>1) {
				warningMessage(event, GUILDS_ROLE_MULTY);
				return;
			}
			
			if(rolesByName.isEmpty()) {
				warningMessage(event, GUILDS_ROLE_NOTEXIST);
				return;
			}
			
			role = rolesByName.get(0);
		}
		
		guild.setIdRoleReserved(ULong.valueOf(role.getIdLong()));
		guild.store();
		
		okMessage(event, GUILDS_ROLE_OK, inputRole);
	}
}