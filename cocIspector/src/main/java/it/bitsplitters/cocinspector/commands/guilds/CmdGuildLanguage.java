package it.bitsplitters.cocinspector.commands.guilds;

import static it.bitsplitters.cocinspector.Config.guildsManager;
import static it.bitsplitters.cocinspector.util.MessageUtil.okMessage;
import static it.bitsplitters.cocinspector.util.MessageUtil.warningMessage;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.GUILDS_LANG;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.GUILDS_LANG_NOTEXIST;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.GUILDS_LANG_OK;

import it.bitsplitters.cocinspector.Config.Languages;
import it.bitsplitters.cocinspector.commands.CoCIspectorCommand;
import it.bitsplitters.cocinspector.jooq.models.tables.records.GuildsRecord;
import it.bitsplitters.command.PermissionLevel;
import it.bitsplitters.command.signature.Signature;
import it.bitsplitters.command.signature.parameter.impl.StringParameter;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class CmdGuildLanguage extends CoCIspectorCommand {

	@Override public PermissionLevel permissionLevel() {
		return PermissionLevel.ADMIN;
	}
	
	@Override public String name() {
		return "guild lang";
	}

	@Override public String helpKey() {
		return GUILDS_LANG;
	}

	@Signature(parameters = StringParameter.class)
	public void command(MessageReceivedEvent event, String lang) {

		if(checkMaintenance(event))
			return;
		
		if(!Languages.labels.contains(lang.toLowerCase())) {
			warningMessage(event, GUILDS_LANG_NOTEXIST);
			return;	
		}
		
		GuildsRecord guild = guildsManager.getGuild(event.getGuild().getIdLong());
		
		guild.setLocale(lang);
		guild.store();
		
		okMessage(event, GUILDS_LANG_OK);
	}
}