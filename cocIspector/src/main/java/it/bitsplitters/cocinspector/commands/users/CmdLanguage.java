package it.bitsplitters.cocinspector.commands.users;

import static it.bitsplitters.cocinspector.Config.usersManager;
import static it.bitsplitters.cocinspector.util.MessageUtil.okMessage;
import static it.bitsplitters.cocinspector.util.MessageUtil.warningMessage;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.USER_LANG;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.USER_LANG_NOTEXIST;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.USER_LANG_OK;

import it.bitsplitters.cocinspector.Config.Languages;
import it.bitsplitters.cocinspector.commands.CoCIspectorCommand;
import it.bitsplitters.cocinspector.jooq.models.tables.records.ProfileRecord;
import it.bitsplitters.command.ChannelEnabled;
import it.bitsplitters.command.PermissionLevel;
import it.bitsplitters.command.signature.Signature;
import it.bitsplitters.command.signature.parameter.impl.StringParameter;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class CmdLanguage extends CoCIspectorCommand {

	@Override public PermissionLevel permissionLevel() {
		return PermissionLevel.PUBLIC;
	}
	
	@Override public String name() {
		return "lang";
	}

	@Override public String helpKey() {
		return USER_LANG;
	}
	
	@Override public ChannelEnabled channelEnabled() {
		return ChannelEnabled.DMBOT;
	}
	
	@Signature(parameters = StringParameter.class)
	public void command(MessageReceivedEvent event, String lang) {

		if(checkMaintenance(event))
			return;
		
		ProfileRecord profile = usersManager.ifAbsentRegisterIt(event.getAuthor().getIdLong());
		
		if(!Languages.labels.contains(lang.toLowerCase())) {
			warningMessage(event, USER_LANG_NOTEXIST);
			return;	
		}
		
		profile.setLocale(lang);
		profile.store();
		
		okMessage(event, USER_LANG_OK);
	}
}
