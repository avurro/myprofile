package it.bitsplitters.cocinspector.commands.guilds;

import static it.bitsplitters.cocinspector.Config.datePattern;
import static it.bitsplitters.cocinspector.Config.getMsg;
import static it.bitsplitters.cocinspector.Config.guildsManager;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.GUILDS_STATUS;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.GUILDS_STATUS_OK;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.replaceValues;
import static it.bitsplitters.util.BotUtil.infoEmbed;

import java.time.format.DateTimeFormatter;

import it.bitsplitters.cocinspector.commands.CoCIspectorCommand;
import it.bitsplitters.cocinspector.jooq.models.tables.records.GuildsRecord;
import it.bitsplitters.command.PermissionLevel;
import it.bitsplitters.command.signature.Signature;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
public class CmdGuildStatus extends CoCIspectorCommand {

	@Override public PermissionLevel permissionLevel() {
		return PermissionLevel.ADMIN;
	}
	
	@Override public String name() {
		return "guild status";
	}

	@Override public String helpKey() {
		return GUILDS_STATUS;
	}

	@Signature public void command(MessageReceivedEvent event) {

		if(checkMaintenance(event))
			return;
		
		GuildsRecord guild = guildsManager.getGuild(event.getGuild().getIdLong());
		
		String reserverRole = guild.getIdRoleReserved() != null
				            ? "<@&"+guild.getIdRoleReserved().longValue()+">" 
				            : "N/A";
		
		String endDate = guild.getPayEnd() != null 
							? guild.getPayEnd().format(DateTimeFormatter.ofPattern(datePattern))
							: "FREE subscription";
							
		String locale = guild.getLocale().toUpperCase();
		String prefix = guild.getPrefix();
		
		infoEmbed(event, replaceValues(getMsg(guild, GUILDS_STATUS_OK), endDate, locale, prefix, reserverRole));
	}
}