package it.bitsplitters.cocinspector.commands.users;

import static it.bitsplitters.cocinspector.Config.jooq;
import static it.bitsplitters.cocinspector.Config.usersManager;
import static it.bitsplitters.cocinspector.jooq.models.tables.Coc.COC;
import static it.bitsplitters.cocinspector.services.AccountService.getDBAccount;
import static it.bitsplitters.cocinspector.util.MessageUtil.errorMessage;
import static it.bitsplitters.cocinspector.util.MessageUtil.exHandling;
import static it.bitsplitters.cocinspector.util.MessageUtil.okMessage;
import static it.bitsplitters.cocinspector.util.MessageUtil.warningMessage;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.LINK_INVALID_API;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.UNLINK;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.UNLINK_KO;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.UNLINK_NOTLINKED_ANYONE;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.UNLINK_OK;

import it.bitsplitters.cocinspector.API;
import it.bitsplitters.cocinspector.commands.CoCIspectorCommand;
import it.bitsplitters.command.ChannelEnabled;
import it.bitsplitters.command.signature.Signature;
import it.bitsplitters.command.signature.parameter.impl.StringParameter;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class CmdUnlink extends CoCIspectorCommand {

	
	@Override public String name() { return "unlink"; }

	@Override public String helpKey() { return UNLINK; }

	@Override public ChannelEnabled channelEnabled() {
		return ChannelEnabled.DMBOT;
	}

	@Signature
	public void unlink(MessageReceivedEvent event) {
		unlink(event, "1");
	}
	
	@Signature(order = 1, parameters = {StringParameter.class})
	public void unlink(MessageReceivedEvent event, String tagOrPosition) {

		if(checkMaintenance(event))
			return;
		
		usersManager.ifAbsentRegisterIt(event.getAuthor().getIdLong());
		
		getDBAccount(event, tagOrPosition)
		.ifPresent((dbAccount) -> {
				dbAccount.delete();
				okMessage(event, UNLINK_OK);
		});
	}
	
	@Signature(order = 2, parameters = {StringParameter.class, StringParameter.class})
	public void unlink(MessageReceivedEvent event, String tag, String token) {

		if(checkMaintenance(event))
			return;
		
		if(isntLinkedToAnyone(tag)) {
			warningMessage(event, UNLINK_NOTLINKED_ANYONE, tag);
			return;
		}

		API.COC.verifyToken(tag, token)
		.whenComplete((playerToken, tokenEx) -> {
			
			if(exHandling(event, tokenEx, tag))
				return;
			
			switch(playerToken.getStatus().toUpperCase()) {
			
			case "INVALID": 
				warningMessage(event, LINK_INVALID_API);
				break;
				
			case "OK": 
				if(deleteTag(tag) >=1) okMessage(event, UNLINK_OK);
				else 				   errorMessage(event, UNLINK_KO);
				break;
			}
		});
	}
	
	private boolean isntLinkedToAnyone(String tag) {
		
		return !jooq().fetchExists(
					jooq().selectFrom(COC).where(COC.TAG.eq(tag))
			   );
	}
	
	private int deleteTag(String tag) {
		return jooq().delete(COC).where(COC.TAG.eq(tag)).execute();
	}
}