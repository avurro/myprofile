package it.bitsplitters.cocinspector.commands;

import static it.bitsplitters.cocinspector.Config.getMsg;
import static it.bitsplitters.cocinspector.Config.getSettings;
import static it.bitsplitters.cocinspector.util.MessageUtil.*;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.*;

import java.util.Optional;

import it.bitsplitters.cocinspector.Config;
import it.bitsplitters.cocinspector.jooq.models.tables.records.GuildsRecord;
import it.bitsplitters.command.ChannelEnabled;
import it.bitsplitters.command.GeneralCommand;
import it.bitsplitters.command.PermissionLevel;
import it.bitsplitters.setting.Settings;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
public abstract class CoCIspectorCommand extends GeneralCommand {

	public static boolean isMaintenance = false;
	public static boolean maintenanceMessage = true;
	
	@Override
	public String helpContent(MessageReceivedEvent event) {
		
		if(permissionLevel() == PermissionLevel.RESERVED 
		&& channelEnabled() == ChannelEnabled.GUILD) {
			
			Optional<GuildsRecord> optGuild = Config.guildsManager.findRecord(event.getGuild().getIdLong());
			
			if(!optGuild.isPresent()) return "";
			
			GuildsRecord guild = optGuild.get();
			
			long roleID = guild.getIdRoleReserved().longValue();
		
			String reserved = getMsg(guild, ROLE_RESERVED);
			
			if(roleID != 0 && event.getGuild().getRoleById(roleID) != null) 
				reserved = event.getGuild().getRoleById(roleID).getAsMention();
			
			
			return replaceValues(getMsg(guild, helpKey()), guild.getPrefix(), name(), reserved);
		}
		
		Optional<? extends Settings> optSettings = getSettings(event);
		
		if(!optSettings.isPresent()) return "";
		
		return replaceValues(getMsg(optSettings.get(), helpKey()), optSettings.get().prefix(), name());
	}

	@Override
	public EmbedBuilder embedHelp(MessageReceivedEvent event) {
		return super.embedHelp(event).setColor(cocIspectorColor);
	}
	
	public abstract String helpKey();
	
	protected boolean checkMaintenance(MessageReceivedEvent event) {
		if(isMaintenance) 
			if(maintenanceMessage) 
				stopMessage(event, GENERIC_APP_MAINTENANCE);
		
		return isMaintenance;
	}
}
