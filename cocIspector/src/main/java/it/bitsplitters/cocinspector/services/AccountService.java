package it.bitsplitters.cocinspector.services;

import static it.bitsplitters.cocinspector.Config.jooq;
import static it.bitsplitters.cocinspector.Config.usersManager;
import static it.bitsplitters.cocinspector.jooq.models.tables.Coc.COC;
import static it.bitsplitters.cocinspector.jooq.models.tables.Profile.PROFILE;
import static it.bitsplitters.cocinspector.util.MessageUtil.exHandling;
import static it.bitsplitters.cocinspector.util.MessageUtil.warningMessage;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.RENAME_PARAMETER_INVALID;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.RENAME_WRONGPARAM;
import static it.bitsplitters.cocinspector.util.ResourceBundleUtil.VIEW_NOACCOUNTS;

import java.util.Optional;

import org.jooq.Record;
import org.jooq.Result;
import org.jooq.types.ULong;

import it.bitsplitters.clashapi.Player;
import it.bitsplitters.cocinspector.API;
import it.bitsplitters.cocinspector.Config;
import it.bitsplitters.cocinspector.jooq.models.tables.records.CocRecord;
import it.bitsplitters.cocinspector.jooq.models.tables.records.ProfileRecord;
import it.bitsplitters.cocinspector.util.MessageUtil;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class AccountService {

	public static Optional<CocRecord> getDBAccount(MessageReceivedEvent event, String tagOrPosition) {
		
		Integer chosedPosition = null;
		boolean isTag = false;
		Optional<CocRecord> accountFound = Optional.empty();

		ProfileRecord profile = usersManager.ifAbsentRegisterIt(event.getAuthor().getIdLong());

		if(tagOrPosition.startsWith("#")) isTag = true;
		
		else {
			
			try {
				chosedPosition = Integer.valueOf(tagOrPosition);
			}catch (NumberFormatException e) {
				warningMessage(event, RENAME_PARAMETER_INVALID);
				return accountFound;
			}
			
		}
		
		
		Result<CocRecord> accounts = getAccounts(profile);
		
		if(accounts.isEmpty()) {
			warningMessage(event, VIEW_NOACCOUNTS);
			return accountFound;
		}
		
		int pos = 1;
		for (CocRecord account : accounts) 
			if(    ( isTag && account.getTag().equals(tagOrPosition)) 
			    || (!isTag && chosedPosition == pos++))
					accountFound = Optional.of(account);
		
		
		if(accountFound.isEmpty()) 
			warningMessage(event, RENAME_WRONGPARAM);
		
		return accountFound;
	}
	
	public static void findAPIAccount(MessageReceivedEvent event, CocRecord dbAccount, AccountsConsumer<MessageReceivedEvent, CocRecord, Player> consumer) {
		findAPIAccount(event, dbAccount, 0, consumer, MessageUtil.sendLoading(event));
	}
	
	private static void findAPIAccount(MessageReceivedEvent event, CocRecord dbAccount, int numberTry, AccountsConsumer<MessageReceivedEvent, CocRecord, Player> consumer, Message loading) {
		
		API.COC.requestPlayer(dbAccount.getTag())
		.whenCompleteAsync((apiAccount, playerEx) -> {
			
			if(playerEx != null)
				if(numberTry < 10) {
					findAPIAccount(event, dbAccount, numberTry+1, consumer, loading);
					return;
				}else {
					exHandling(event, playerEx, dbAccount.getTag());
					loading.delete();
					return;
				}
					
			loading.delete();
			consumer.accept(event, dbAccount, apiAccount);
			
		});
	}

	public static void findAPIAccount(MessageReceivedEvent event, String tag, AccountApiConsumer<MessageReceivedEvent, Player> consumer) {
		findAPIAccount(event, tag, 0, consumer, MessageUtil.sendLoading(event));
	}
		
	private static void findAPIAccount(MessageReceivedEvent event, String tag, int numberTry, AccountApiConsumer<MessageReceivedEvent, Player> consumer, Message loading) {
		
		API.COC.requestPlayer(tag)
		.whenCompleteAsync((apiAccount, playerEx) -> {
			
			if(playerEx != null)
				if(numberTry < 6) {
					findAPIAccount(event, tag, numberTry+1, consumer, loading);
					return;
				}else {
					exHandling(event, playerEx, tag);
					loading.delete();
					return;
				}
					
			loading.delete();
			consumer.accept(event, apiAccount);
			
		});
	}
	
	public static ProfileRecord getProfile(Long discordID) {
		return jooq().selectFrom(PROFILE).where(PROFILE.ID_DISCORD.eq(ULong.valueOf(discordID))).fetchOne();
	}
	
	public static ProfileRecord getProfile(Integer profileID) {
		return jooq().selectFrom(PROFILE).where(PROFILE.ID_PROFILE.eq(profileID)).fetchOne();
	}
	
	public static Result<CocRecord> getAccounts(ProfileRecord profile){
		return jooq().selectFrom(COC)
				     .where(COC.ID_PROFILE.eq(profile.getIdProfile()))
				     .orderBy(COC.TH_LEVEL.desc()).fetch();
	}
	
	public static Result<Record> getAccounts(Long discordID){
		return jooq().select(COC.fields()).from(COC).naturalJoin(PROFILE)
				     .where(PROFILE.ID_DISCORD.eq(ULong.valueOf(discordID))
				     .and(PROFILE.ID_APP.eq(Config.appID)))
				     .orderBy(COC.TH_LEVEL.desc()).fetch();
	}
	
	public static CocRecord getCocAccount(String tag) {
		return jooq().selectFrom(COC).where(COC.TAG.eq(tag)).fetchOne();
	}
}