package it.bitsplitters.cocinspector.services;

/**
 * @author AVurro
 *
 * @param <Event> Reference to the user request
 * @param <DB> Account data retrieved from DB call
 * @param <API> Account data retrieved from API call
 */
@FunctionalInterface
public interface AccountsConsumer<Event, DB, API> {

    /**
     * Performs this operation on the given arguments.
     * @param event
     * @param dbAccountData
     * @param apiAccountData
     */
    void accept(Event event, DB dbAccountData, API apiAccountData);
}
