package it.bitsplitters.cocinspector;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.bitsplitters.clashapi.ClashAPIAsync;
import it.bitsplitters.clashapi.ClashWrapper;
import it.bitsplitters.clashapi.Player;
import it.bitsplitters.clashapi.PlayerToken;

public enum API {

	COC;
	
	private static final Logger logger = LogManager.getLogger();
	
	private ClashAPIAsync clashAPIAsync;
	private String[] apiToken = new String [] {
			
		//COC ISPECTOR 1
		"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6IjY2OWVlODcxLWEwNGYtNGVhYy1iMDc4LTAzNjBmZWU1NzczOCIsImlhdCI6MTYxNTAzOTg4OSwic3ViIjoiZGV2ZWxvcGVyL2Q5MTEzNGMwLWM5ODYtMGQzZS1kYzk3LTc1NTgyMzIwYTc1MiIsInNjb3BlcyI6WyJjbGFzaCJdLCJsaW1pdHMiOlt7InRpZXIiOiJkZXZlbG9wZXIvc2lsdmVyIiwidHlwZSI6InRocm90dGxpbmcifSx7ImNpZHJzIjpbIjkzLjM3LjEzMy4xNzciXSwidHlwZSI6ImNsaWVudCJ9XX0.FCQ57CAdf3rjZGiKGo4Hc132bE1ipu58RzDYE7saI85csWifogr54Bi4Mge6sD0Zaed-7CEP0BHjSqS7X4xSrQ",
		
		//COC ISPECTOR 2
		"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6ImM3MWFmNzFjLTAyOTktNDU4Ni04NTA5LTY2MmQ2YzUxNTliYiIsImlhdCI6MTYxNTAzOTg5OSwic3ViIjoiZGV2ZWxvcGVyL2Q5MTEzNGMwLWM5ODYtMGQzZS1kYzk3LTc1NTgyMzIwYTc1MiIsInNjb3BlcyI6WyJjbGFzaCJdLCJsaW1pdHMiOlt7InRpZXIiOiJkZXZlbG9wZXIvc2lsdmVyIiwidHlwZSI6InRocm90dGxpbmcifSx7ImNpZHJzIjpbIjkzLjM3LjEzMy4xNzciXSwidHlwZSI6ImNsaWVudCJ9XX0.Xo_Pf6uyZ2_oUj7gzRzbSxlYFck_fO2N0FwQEDiAXzl2SNS8uY8T6LyVQI__qKMQ1SguFXlc2lpDjziQgq089Q",
		
		//COC ISPECTOR 3
		"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6ImVhZGIxZmIzLTQxM2UtNDcwZC05Y2U1LThlNjVmZTBhZThmYiIsImlhdCI6MTYxNTAzOTkxMCwic3ViIjoiZGV2ZWxvcGVyL2Q5MTEzNGMwLWM5ODYtMGQzZS1kYzk3LTc1NTgyMzIwYTc1MiIsInNjb3BlcyI6WyJjbGFzaCJdLCJsaW1pdHMiOlt7InRpZXIiOiJkZXZlbG9wZXIvc2lsdmVyIiwidHlwZSI6InRocm90dGxpbmcifSx7ImNpZHJzIjpbIjkzLjM3LjEzMy4xNzciXSwidHlwZSI6ImNsaWVudCJ9XX0.Bpq_l1R77-C98jUiL_MD2IwrRk4i4y-YnP4n8CatYMaFTrdLqlB8XmPhWuZF_qTvqkmK5HYCGpBTGW_pP7QT6A",
		
		//COC ISPECTOR 4
		"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6Ijk5ZWY2NWJiLTA5NDItNDIxYy05MDkzLTVhZjg1MTVmMTdjNCIsImlhdCI6MTYxNTAzOTkyNCwic3ViIjoiZGV2ZWxvcGVyL2Q5MTEzNGMwLWM5ODYtMGQzZS1kYzk3LTc1NTgyMzIwYTc1MiIsInNjb3BlcyI6WyJjbGFzaCJdLCJsaW1pdHMiOlt7InRpZXIiOiJkZXZlbG9wZXIvc2lsdmVyIiwidHlwZSI6InRocm90dGxpbmcifSx7ImNpZHJzIjpbIjkzLjM3LjEzMy4xNzciXSwidHlwZSI6ImNsaWVudCJ9XX0.ceoVrjCNN_-jCbVZL5fN489F0EX_e1cdUALLiGMgNpEyF53CNQiJCj6INwCNS8xqhXFg3oCIdu9CGucMXUftww"
	};
	
	int tokenNumber = 0;
	
	private synchronized String getApi() {
		tokenNumber ++;
		if(tokenNumber >= 4)
			tokenNumber = 0;
		
		return apiToken[tokenNumber];
	}
	
	private API() {
		clashAPIAsync = ClashWrapper.getAPIInstanceAsync(this::getApi);
	}
	
	public ClashAPIAsync getAsync() {
		return this.clashAPIAsync;
	}
	
	public CompletableFuture<Player> requestPlayer(String playerTag){
		
		try {
		
			return getAsync().requestPlayer(playerTag);
		
		} catch (CompletionException | UnsupportedEncodingException | URISyntaxException e) {
			logger.atError().log(e.toString());
			return null;
		}
	}
	
	public CompletableFuture<PlayerToken> verifyToken(String playerTag, String token){
		try {
			
			return getAsync().verifyToken(playerTag, token);
		
		} catch (CompletionException | UnsupportedEncodingException | URISyntaxException e) {
			logger.atError().log(e.toString());
			return null;
		}
	}
}