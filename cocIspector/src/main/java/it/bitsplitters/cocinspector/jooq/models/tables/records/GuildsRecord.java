/*
 * This file is generated by jOOQ.
 */
package it.bitsplitters.cocinspector.jooq.models.tables.records;


import it.bitsplitters.cocinspector.jooq.models.tables.Guilds;

import java.time.LocalDate;

import org.jooq.Field;
import org.jooq.Record2;
import org.jooq.Record7;
import org.jooq.Row7;
import org.jooq.impl.UpdatableRecordImpl;
import org.jooq.types.ULong;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class GuildsRecord extends UpdatableRecordImpl<GuildsRecord> implements Record7<ULong, Byte, Byte, LocalDate, String, String, ULong> {

    private static final long serialVersionUID = -907053808;

    /**
     * Setter for <code>myprofile.guilds.ID_GUILD</code>.
     */
    public void setIdGuild(ULong value) {
        set(0, value);
    }

    /**
     * Getter for <code>myprofile.guilds.ID_GUILD</code>.
     */
    public ULong getIdGuild() {
        return (ULong) get(0);
    }

    /**
     * Setter for <code>myprofile.guilds.ID_APP</code>.
     */
    public void setIdApp(Byte value) {
        set(1, value);
    }

    /**
     * Getter for <code>myprofile.guilds.ID_APP</code>.
     */
    public Byte getIdApp() {
        return (Byte) get(1);
    }

    /**
     * Setter for <code>myprofile.guilds.PAY_LEVEL</code>.
     */
    public void setPayLevel(Byte value) {
        set(2, value);
    }

    /**
     * Getter for <code>myprofile.guilds.PAY_LEVEL</code>.
     */
    public Byte getPayLevel() {
        return (Byte) get(2);
    }

    /**
     * Setter for <code>myprofile.guilds.PAY_END</code>.
     */
    public void setPayEnd(LocalDate value) {
        set(3, value);
    }

    /**
     * Getter for <code>myprofile.guilds.PAY_END</code>.
     */
    public LocalDate getPayEnd() {
        return (LocalDate) get(3);
    }

    /**
     * Setter for <code>myprofile.guilds.PREFIX</code>.
     */
    public void setPrefix(String value) {
        set(4, value);
    }

    /**
     * Getter for <code>myprofile.guilds.PREFIX</code>.
     */
    public String getPrefix() {
        return (String) get(4);
    }

    /**
     * Setter for <code>myprofile.guilds.LOCALE</code>.
     */
    public void setLocale(String value) {
        set(5, value);
    }

    /**
     * Getter for <code>myprofile.guilds.LOCALE</code>.
     */
    public String getLocale() {
        return (String) get(5);
    }

    /**
     * Setter for <code>myprofile.guilds.ID_ROLE_RESERVED</code>.
     */
    public void setIdRoleReserved(ULong value) {
        set(6, value);
    }

    /**
     * Getter for <code>myprofile.guilds.ID_ROLE_RESERVED</code>.
     */
    public ULong getIdRoleReserved() {
        return (ULong) get(6);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record2<ULong, Byte> key() {
        return (Record2) super.key();
    }

    // -------------------------------------------------------------------------
    // Record7 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row7<ULong, Byte, Byte, LocalDate, String, String, ULong> fieldsRow() {
        return (Row7) super.fieldsRow();
    }

    @Override
    public Row7<ULong, Byte, Byte, LocalDate, String, String, ULong> valuesRow() {
        return (Row7) super.valuesRow();
    }

    @Override
    public Field<ULong> field1() {
        return Guilds.GUILDS.ID_GUILD;
    }

    @Override
    public Field<Byte> field2() {
        return Guilds.GUILDS.ID_APP;
    }

    @Override
    public Field<Byte> field3() {
        return Guilds.GUILDS.PAY_LEVEL;
    }

    @Override
    public Field<LocalDate> field4() {
        return Guilds.GUILDS.PAY_END;
    }

    @Override
    public Field<String> field5() {
        return Guilds.GUILDS.PREFIX;
    }

    @Override
    public Field<String> field6() {
        return Guilds.GUILDS.LOCALE;
    }

    @Override
    public Field<ULong> field7() {
        return Guilds.GUILDS.ID_ROLE_RESERVED;
    }

    @Override
    public ULong component1() {
        return getIdGuild();
    }

    @Override
    public Byte component2() {
        return getIdApp();
    }

    @Override
    public Byte component3() {
        return getPayLevel();
    }

    @Override
    public LocalDate component4() {
        return getPayEnd();
    }

    @Override
    public String component5() {
        return getPrefix();
    }

    @Override
    public String component6() {
        return getLocale();
    }

    @Override
    public ULong component7() {
        return getIdRoleReserved();
    }

    @Override
    public ULong value1() {
        return getIdGuild();
    }

    @Override
    public Byte value2() {
        return getIdApp();
    }

    @Override
    public Byte value3() {
        return getPayLevel();
    }

    @Override
    public LocalDate value4() {
        return getPayEnd();
    }

    @Override
    public String value5() {
        return getPrefix();
    }

    @Override
    public String value6() {
        return getLocale();
    }

    @Override
    public ULong value7() {
        return getIdRoleReserved();
    }

    @Override
    public GuildsRecord value1(ULong value) {
        setIdGuild(value);
        return this;
    }

    @Override
    public GuildsRecord value2(Byte value) {
        setIdApp(value);
        return this;
    }

    @Override
    public GuildsRecord value3(Byte value) {
        setPayLevel(value);
        return this;
    }

    @Override
    public GuildsRecord value4(LocalDate value) {
        setPayEnd(value);
        return this;
    }

    @Override
    public GuildsRecord value5(String value) {
        setPrefix(value);
        return this;
    }

    @Override
    public GuildsRecord value6(String value) {
        setLocale(value);
        return this;
    }

    @Override
    public GuildsRecord value7(ULong value) {
        setIdRoleReserved(value);
        return this;
    }

    @Override
    public GuildsRecord values(ULong value1, Byte value2, Byte value3, LocalDate value4, String value5, String value6, ULong value7) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached GuildsRecord
     */
    public GuildsRecord() {
        super(Guilds.GUILDS);
    }

    /**
     * Create a detached, initialised GuildsRecord
     */
    public GuildsRecord(ULong idGuild, Byte idApp, Byte payLevel, LocalDate payEnd, String prefix, String locale, ULong idRoleReserved) {
        super(Guilds.GUILDS);

        set(0, idGuild);
        set(1, idApp);
        set(2, payLevel);
        set(3, payEnd);
        set(4, prefix);
        set(5, locale);
        set(6, idRoleReserved);
    }
}
