package it.bitsplitters.cocinspector;

import static it.bitsplitters.cocinspector.Config.*;
import static it.bitsplitters.cocinspector.adapters.Adapters.*;
import static it.bitsplitters.cocinspector.jooq.models.tables.Profile.PROFILE;

import java.util.Optional;

import org.jooq.types.ULong;

import it.bitsplitters.cocinspector.jooq.models.tables.records.ProfileRecord;
import it.bitsplitters.setting.UserSettings;

public class UsersManager implements it.bitsplitters.UsersManager {

	@Override
	public Optional<UserSettings> find(Long userID) {
		Optional<ProfileRecord> optUser = findRecord(userID);
		
		if(!optUser.isPresent()) return Optional.empty();
		
		return Optional.of(toUser(optUser.get()));
	}

	@Override
	public UserSettings register(Long userID) {
		ProfileRecord profile = new ProfileRecord();
		profile.setLocale(locale);
		profile.setPrefix(defaultPrefix);
		profile.setIdApp(appID);
		profile.setIdDiscord(ULong.valueOf(userID));
		jooq().executeInsert(profile);
		
		return toUser(profile);
	}

	public Optional<ProfileRecord> findRecord(Long userID){
		return	jooq().selectFrom(PROFILE)
					  .where(PROFILE.ID_DISCORD.eq(ULong.valueOf(userID))
					  .and(PROFILE.ID_APP.eq(Config.appID)))
					  .fetchOptional();
	}
	
	public ProfileRecord ifAbsentRegisterIt(Long userID) {
		
		Optional<ProfileRecord> optUser = findRecord(userID);
		
		if(optUser.isPresent()) return optUser.get();
		else {
			register(userID);
			return findRecord(userID).get();
		}
	}
}