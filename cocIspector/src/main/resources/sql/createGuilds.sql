CREATE DATABASE myprofile;

----------------------------------------------
--				APP
----------------------------------------------
CREATE TABLE app (
ID_APP TINYINT NOT NULL PRIMARY KEY,
NAME VARCHAR(20) NOT NULL
);

----------------------------------------------
--				GUILDS
----------------------------------------------
CREATE TABLE guilds (
ID_GUILD BIGINT UNSIGNED NOT NULL,
ID_APP TINYINT NOT NULL,
PAY_LEVEL TINYINT DEFAULT 0,
PAY_END DATE,
PREFIX VARCHAR(3) NOT NULL DEFAULT "-",
LOCALE VARCHAR(2) NOT NULL DEFAULT "EN",
ID_ROLE_RESERVED BIGINT UNSIGNED,
PRIMARY KEY (ID_GUILD, ID_APP),
FOREIGN KEY (ID_APP) REFERENCES app (ID_APP) ON DELETE CASCADE ON UPDATE RESTRICT
);

----------------------------------------------
--				PROFILE
----------------------------------------------
CREATE TABLE profile (
ID_PROFILE MEDIUMINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
ID_DISCORD BIGINT UNSIGNED NOT NULL,
ID_APP TINYINT NOT NULL,
PREFIX VARCHAR(3) NOT NULL DEFAULT "-",
LOCALE VARCHAR(2) NOT NULL DEFAULT "EN",
CONSTRAINT UC_Profile UNIQUE (ID_DISCORD, ID_APP),
FOREIGN KEY (ID_APP) REFERENCES app (ID_APP) ON DELETE CASCADE ON UPDATE RESTRICT
);

----------------------------------------------
--				COC ISPECTOR
----------------------------------------------
CREATE TABLE coc (
ID_PROFILE MEDIUMINT NOT NULL,
TAG VARCHAR(10) NOT NULL UNIQUE,
NICKNAME VARCHAR(20) NOT NULL,
TH_LEVEL TINYINT NOT NULL,
PRIMARY KEY (ID_PROFILE, TAG),
FOREIGN KEY (ID_PROFILE) REFERENCES profile (ID_PROFILE) ON DELETE CASCADE ON UPDATE RESTRICT
);

----------------------------------------------
--				VERIFIED USER
----------------------------------------------
CREATE TABLE coc_verified_user (
ID_GUILD BIGINT UNSIGNED,
ID_ROLE BIGINT UNSIGNED NOT NULL,
SIGN TINYINT(1) NOT NULL,
FOREIGN KEY (ID_GUILD) REFERENCES guilds (ID_GUILD) ON DELETE CASCADE ON UPDATE RESTRICT
);

----------------------------------------------
--				CLAN ROLES
----------------------------------------------
CREATE TABLE coc_clan_roles (
ID_GUILD BIGINT UNSIGNED,
TAG VARCHAR(10) NOT NULL,
ID_ROLE BIGINT UNSIGNED NOT NULL,
SIGN TINYINT(1) NOT NULL,
CONSTRAINT GUILD_TAG_ROLE UNIQUE (ID_GUILD, TAG, ID_ROLE),
FOREIGN KEY (ID_GUILD) REFERENCES guilds (ID_GUILD) ON DELETE CASCADE ON UPDATE RESTRICT
);
----------------------------------------------
--				FEDERESPORTS
----------------------------------------------
CREATE TABLE federesports (
ID_PROFILE MEDIUMINT NOT NULL PRIMARY KEY,
TESSERA MEDIUMINT NOT NULL UNIQUE,
SCADENZA DATE NOT NULL,
FOREIGN KEY (ID_PROFILE) REFERENCES profile (ID_PROFILE) ON DELETE CASCADE ON UPDATE RESTRICT
);

----------------------------------------------
--				INSERTS
----------------------------------------------
INSERT INTO app VALUES (0, 'COC_ISPECTOR');
