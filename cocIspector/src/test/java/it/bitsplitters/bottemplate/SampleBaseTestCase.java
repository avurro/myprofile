package it.bitsplitters.bottemplate;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.MockitoAnnotations;

public class SampleBaseTestCase {
	private AutoCloseable closeable;
	private ByteArrayOutputStream baos;
	private PrintStream old;
	
    @BeforeEach public void before() {
        closeable = MockitoAnnotations.openMocks(this);
        
        // IMPORTANT: Save the old System.out!
        old = System.out;
        // Create a stream to hold the output
        baos = new ByteArrayOutputStream();
        // Tell Java to use your special stream
        System.setOut(new PrintStream(baos));
       
    }

    @AfterEach public void after() throws Exception {
        closeable.close();
        baos.close();
        
        System.setOut(old);
        System.out.flush();
    }

	public String getResult() {
		return baos.toString();
	}
}
